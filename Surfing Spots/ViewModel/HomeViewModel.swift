//
//  HomeViewModel.swift
//  Surfing Spots
//
//  Created by Alessandro Graziani on 29/07/2020.
//  Copyright © 2020 Alessandro Graziani. All rights reserved.
//

import Foundation

class HomeViewModel {
    
    private var cities: [City]? {
        willSet {
            citiesUpdating?(newValue)
        }
    }
    
    private var timer: Timer?
    
    deinit {
        self.timer?.invalidate()
    }
    
    // MARK: Events closures
    var citiesUpdating: (([City]?) -> Void)?
    var errorUpdating: ((GenericError?) -> Void)?
    var orderUpdating: ((City) -> Void)?
    var updatePictureAt: ((Int) -> Void)?
    
    // MARK: Repositories
    private lazy var mainRepository: MainRepository? = {
        DependencyResolver.resolve(MainRepository.self)
    }()
    private lazy var photoRepository: PhotoRepository? = {
        PhotoRepository()
    }()
    
    func fetchData() {
        
        self.mainRepository?.retrieveCities { [weak self] result in
            switch result {
                
            case .success(let cities):
                self?.cities = (cities.cities.map { City(name: $0.name) }).sorted(by: { $0.degrees > $1.degrees})
                self?.fetchPicture()
                self?.startFetchingDegrees()
            case .failure(let error):
                self?.errorUpdating?(error)
            }
        }
    }
    
    
    // MARK: Private Methods
    private func fetchPicture() {
        self.cities?.forEach { [weak self] city in
            self?.photoRepository?.getPhotoFor(city: city.name, completionHandler: { result in
                if case .success(let image) = result, let index = self?.cities?.firstIndex(where: { $0 == city}) {
                    city.image = image
                    self?.updatePictureAt?(index)
                }
            })
        }
    }
    
    private func startFetchingDegrees() {
        
        self.timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(fetchDegrees), userInfo: nil, repeats: true)
    }
    
    @objc private func fetchDegrees() {
        self.mainRepository?.retrieveRandomNumber { [weak self] result in
            switch result {
            case .success(let number):
                self?.updateRandomDegrees(value: number)
            case .failure(let error):
                debugPrint(error.message)
            }
        }
    }
    
    private func updateRandomDegrees(value: Int) {
        guard let cities = self.cities, cities.count > 0 else { return }
        
        let indexUpdating = Int.random(in: 0..<cities.count)
        cities[indexUpdating].updateDegrees(newValue: value)
        self.orderUpdating?(cities[indexUpdating])
    }

}
