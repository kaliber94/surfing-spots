//
//  City.swift
//  Surfing Spots
//
//  Created by Alessandro Graziani on 29/07/2020.
//  Copyright © 2020 Alessandro Graziani. All rights reserved.
//

import UIKit

class City {
    
    var name: String
    var image: UIImage?
    var degrees: Int
    private var climate: Climate
    
    init(name: String) {
        self.name = name
        self.degrees = Int.random(in: 10...40)
        self.climate = Climate(degrees: self.degrees)
    }
    
    func getClimate() -> String {
        self.climate.rawValue
    }
    
    func getDegrees() -> String {
        "\(self.degrees) degrees"
    }
    
    func updateDegrees(newValue: Int) {
        self.degrees = newValue
        self.climate = Climate(degrees: self.degrees)
    }
    
    static func == (lhs: City, rhs: City) -> Bool {
           return lhs.name == rhs.name
       }
}
