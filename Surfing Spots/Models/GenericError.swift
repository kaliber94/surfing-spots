//
//  GenericError.swift
//  Surfing Spots
//
//  Created by Alessandro Graziani on 30/07/2020.
//  Copyright © 2020 Alessandro Graziani. All rights reserved.
//

import Foundation

struct GenericError: Error {
    let message: String
    
    init(message: String = "A generic error has occurred, please try again.") {
        self.message = message
    }
}
