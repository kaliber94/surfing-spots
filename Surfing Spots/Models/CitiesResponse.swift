//
//  Cities.swift
//  Surfing Spots
//
//  Created by Alessandro Graziani on 29/07/2020.
//  Copyright © 2020 Alessandro Graziani. All rights reserved.
//

import UIKit

struct CitiesResponse: Decodable {
    
    struct City: Decodable {
        var name: String
    }

    var cities: [City]
}
