//
//  Climate.swift
//  Surfing Spots
//
//  Created by Alessandro Graziani on 29/07/2020.
//  Copyright © 2020 Alessandro Graziani. All rights reserved.
//

import Foundation

enum Climate: String {
    case sunny = "Sunny"
    case cloudy = "Cloudy"
    
    init(degrees: Int) {
        let value: Climate = (degrees >= 30) ? .sunny : .cloudy
        self = value
    }
}
