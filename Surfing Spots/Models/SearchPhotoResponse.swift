//
//  SearchPhotoResponse.swift
//  Surfing Spots
//
//  Created by Alessandro Graziani on 30/07/2020.
//  Copyright © 2020 Alessandro Graziani. All rights reserved.
//

import Foundation

struct PhotoSearchResponse: Decodable {
    
    var results: [PhotoResult]
        
    struct PhotoResult: Decodable {
        var id: String
        var urls: Urls
    }
    
    struct Urls: Decodable {
        var small: String
    }
}
