//
//  PhotoRepository.swift
//  Surfing Spots
//
//  Created by Alessandro Graziani on 30/07/2020.
//  Copyright © 2020 Alessandro Graziani. All rights reserved.
//

import UIKit

class PhotoRepository {
    
    // MARK: API for search & download city image
    func getPhotoFor(city: String, completionHandler: @escaping (Result<UIImage, GenericError>) -> Void) {
        guard var components = URLComponents(string: "https://api.unsplash.com/search/photos") else { return }
        components.queryItems = [
            URLQueryItem(name: "client_id", value: "EU3y7R6mXm39L9WUZ8eam3zyz0eOgkpxLnR-p1XclrU"),
            URLQueryItem(name: "query", value: city)
        ]
        var request = URLRequest(url: components.url!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        session.configuration.timeoutIntervalForRequest = 10
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            
            if let data = data, let photoSearchResponse = try? JSONDecoder().decode(PhotoSearchResponse.self, from: data), let urlImage = photoSearchResponse.results.first?.urls.small, let url = URL(string: urlImage), let imageData = try? Data(contentsOf: url), let image = UIImage(data: imageData) {
                
                DispatchQueue.main.async {
                    completionHandler(.success(image))
                }
                
            } else {
                completionHandler(.failure(GenericError()))
            }
        })

        task.resume()
    }
    
}
