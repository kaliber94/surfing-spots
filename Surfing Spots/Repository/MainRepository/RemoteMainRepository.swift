//
//  RemoteMainRepository.swift
//  Surfing Spots
//
//  Created by Alessandro Graziani on 29/07/2020.
//  Copyright © 2020 Alessandro Graziani. All rights reserved.
//

import UIKit

class RemoteMainRepository: MainRepository {
    
    func retrieveCities(completionHandler: @escaping (Result<CitiesResponse, GenericError>) -> Void) {
        
        guard let url = URL(string: "https://run.mocky.io/v3/652ceb94-b24e-432b-b6c5-8a54bc1226b6") else {
            completionHandler(.failure(GenericError(message: "Invalid URL")))
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, _, _ in
            
            DispatchQueue.main.async {
                if let data = data, let cities = try? JSONDecoder().decode(CitiesResponse.self, from: data) {
                    completionHandler(.success(cities))
                } else {
                    completionHandler(.failure(GenericError()))
                }
            }
        }
        task.resume()
        
    }

    func retrieveRandomNumber(completionHandler: @escaping (Result<Int, GenericError>) -> Void) {
        DispatchQueue.global(qos: .background).async {
            guard let url = URL(string: "http://numbersapi.com/random/math"), let text = try? String(contentsOf: url) else {
                completionHandler(.failure(GenericError()))
                return
            }
            
            let regex = NSRegularExpression("(\\d+(?>\\.\\d+)*)\\w+?(\\d+)")
            if let numberString = regex.stringMatched(in: text), let number = Int(numberString) {
                DispatchQueue.main.async {
                    completionHandler(.success(number))
                }
            } else {
                completionHandler(.failure(GenericError(message: "Error parsing data.")))
            }
        }
    }
    
}
