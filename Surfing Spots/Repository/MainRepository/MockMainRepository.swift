//
//  TestMainRepository.swift
//  Surfing Spots
//
//  Created by Alessandro Graziani on 30/07/2020.
//  Copyright © 2020 Alessandro Graziani. All rights reserved.
//

import UIKit

class MockMainRepository: MainRepository {
    
    func retrieveCities(completionHandler: @escaping (Result<CitiesResponse, GenericError>) -> Void) {
        
        guard let path = Bundle.main.path(forResource: "CityTestDataSource", ofType: "plist"), let data = try? Data(contentsOf: URL(fileURLWithPath: path)), let cities = try? PropertyListSerialization.propertyList(from: data, options: .mutableContainers, format: nil) as? [String] else {
            
            completionHandler(.failure(GenericError()))
            return
        }
        
        completionHandler(.success(CitiesResponse(cities: cities.map { CitiesResponse.City(name: $0)})))
    }
    
    func retrieveRandomNumber(completionHandler: @escaping (Result<Int, GenericError>) -> Void) {
        completionHandler(.success(Int.random(in: 0...5000)))
    }
    
}
