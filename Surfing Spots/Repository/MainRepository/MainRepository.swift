//
//  MainRepository.swift
//  Surfing Spots
//
//  Created by Alessandro Graziani on 29/07/2020.
//  Copyright © 2020 Alessandro Graziani. All rights reserved.
//

import UIKit

protocol MainRepository: class {

    // MARK: API Surfing Spot Challenge
    func retrieveCities(completionHandler: @escaping (Result<CitiesResponse, GenericError>) -> Void)
    func retrieveRandomNumber(completionHandler: @escaping (Result<Int, GenericError>) -> Void)
}
