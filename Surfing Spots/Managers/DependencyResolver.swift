//
//  DependencyResolver.swift
//  Surfing Spots
//
//  Created by Alessandro Graziani on 30/07/2020.
//  Copyright © 2020 Alessandro Graziani. All rights reserved.
//

import Foundation

class DependencyResolver: NSObject {
    
    enum Dependency: String {
        case mainRepository = "MainRepository"
    }
    
    static var map: [Dependency: AnyClass] = [:]
    
    static func inject(_ clazz: AnyClass, for dependency: Dependency) {
        self.map[dependency] = clazz
    }
    
    static func resolve<T: Any>(_ type: T.Type) -> T? {
        let string = String(describing: type)
        if let dependency = Dependency(rawValue: string) {
            return class_createInstance(self.map[dependency], 0) as? T
        }
        return nil
    }
}
