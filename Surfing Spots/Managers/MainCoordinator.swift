//
//  MainCoordinator.swift
//  Surfing Spots
//
//  Created by Alessandro Graziani on 30/07/2020.
//  Copyright © 2020 Alessandro Graziani. All rights reserved.
//

import UIKit

class MainCoordinator {
    
    let rootWindow: UIWindow
    
    init(withWindow rootWindow: UIWindow) {
        self.rootWindow = rootWindow
    }
    
    func start() {
        let viewController = HomeViewController()
        viewController.viewModel = HomeViewModel()
        let navController = UINavigationController(rootViewController: viewController)
        navController.navigationBar.prefersLargeTitles = true
        self.rootWindow.rootViewController = navController
        self.rootWindow.makeKeyAndVisible()
    }
}
