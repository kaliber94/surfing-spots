//
//  CityTableViewCell.swift
//  Surfing Spots
//
//  Created by Alessandro Graziani on 29/07/2020.
//  Copyright © 2020 Alessandro Graziani. All rights reserved.
//

import UIKit

class CityTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var backgroundImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var climateLabel: UILabel!
    @IBOutlet private weak var degreesLabel: UILabel!
    
    // MARK: Overriden Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundImageView.backgroundColor = UIColor.gray
        [self.titleLabel, self.climateLabel, self.degreesLabel].forEach {
            $0?.layer.shadowColor = UIColor.black.cgColor
            $0?.layer.shadowOpacity = 1
            $0?.layer.shadowRadius = 4
            $0?.layer.shadowOffset = CGSize(width: 1, height: 1)
        }
    }
    
    
    // MARK: Setup Methods
    
    func configure(with city: City) {
        self.backgroundImageView.image = city.image
        self.titleLabel.text = city.name
        self.climateLabel.text = city.getClimate()
        self.degreesLabel.text = city.getDegrees()
        
        if city.image != nil {
            self.backgroundImageView.image = city.image
        }
    }
    
    func updateDegrees(newValue: Int) {
        self.degreesLabel.setTextWithTransition("\(newValue) degrees")
        self.climateLabel.setTextWithTransition(Climate(degrees: newValue).rawValue)
    }
    
    func updatePicture(_ picture: UIImage?) {
        self.backgroundImageView.image = picture
    }
    
}
