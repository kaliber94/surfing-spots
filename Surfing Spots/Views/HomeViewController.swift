//
//  HomeViewController.swift
//  Surfing Spots
//
//  Created by Alessandro Graziani on 29/07/2020.
//  Copyright © 2020 Alessandro Graziani. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    
    var viewModel: HomeViewModel?
    var cities: [City]?
    
    private let loader: UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView(style: .large)
        loader.translatesAutoresizingMaskIntoConstraints = false
        loader.hidesWhenStopped = true
        return loader
    }()
    
    // MARK: Initialize
    
    init() {
        super.init(nibName: String(describing: Self.self), bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    // MARK: Overriden Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Surfing Spots"
        self.setupTableView()
        self.setupBindings()
        
        self.view.addSubview(self.loader)
        self.loader.startAnimating()
        NSLayoutConstraint.activate([self.loader.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
                                     self.loader.centerYAnchor.constraint(equalTo: self.view.centerYAnchor)])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.viewModel?.fetchData()
    }

    // MARK: Setup Methods
    
    private func setupTableView() {
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .none
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 216
        self.tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: UIApplication.shared.windows.first?.safeAreaInsets.bottom ?? 0, right: 0) 
        self.tableView.register(UINib(nibName: String(describing: CityTableViewCell.self), bundle: Bundle.main), forCellReuseIdentifier: "cityCell")
    }
    
    private func setupBindings() {
        
        self.viewModel?.citiesUpdating = { [weak self] cities in
            guard let self = self else { return }
            self.loader.stopAnimating()
            self.cities = cities
            self.tableView.reloadSections(IndexSet(integer: 0), with: .bottom)
        }
        
        self.viewModel?.errorUpdating = { [weak self] error in
            self?.loader.stopAnimating()
            let popup = UIAlertController(title: "Error", message: error?.message, preferredStyle: .alert)
            popup.addAction(UIAlertAction(title: "Retry", style: .cancel, handler: { [weak popup] action in
                popup?.dismiss(animated: true, completion: { [weak self] in
                    self?.loader.startAnimating()
                    self?.viewModel?.fetchData()
                })
            }))
            self?.present(popup, animated: true, completion: nil)
        }
        
        self.viewModel?.updatePictureAt = { [weak self] index in
            self?.cellAt(index: index)?.updatePicture(self?.cities?[index].image)
        }
        
        self.viewModel?.orderUpdating = { [weak self] city in
            self?.rearrangeTableView(city: city)
        }
        
    }
    
    private func rearrangeTableView(city: City) {
        guard let cities = self.cities, let index = self.cities?.firstIndex(where: { $0 == city }) else { return }
        
        self.cellAt(index: index)?.updateDegrees(newValue: cities[index].degrees)

        let sortedCities = cities.sorted(by: { $0.degrees > $1.degrees })
                    
        self.tableView.beginUpdates()
        for i in 0..<cities.count {
            let newRow = sortedCities.firstIndex(where: { $0 == cities[i] })!
            self.tableView.moveRow(at: IndexPath(row: i, section: 0), to: IndexPath(row: newRow, section: 0))
        }
        self.cities = sortedCities
        self.tableView.endUpdates()
        
        self.cities?.forEach { print($0.name, " ", $0.getDegrees())}
        print("\n\n")
    }

    
    private func cellAt(index: Int) -> CityTableViewCell? {
        self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? CityTableViewCell
    }
}


extension HomeViewController: UITableViewDataSource {
    
    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cities?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cityCell") as? CityTableViewCell, let city = self.cities?[indexPath.row] else { return UITableViewCell() }
        
        cell.configure(with: city)
        
        return cell
    }
}

