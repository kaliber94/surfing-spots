//
//  UILabel+Transition.swift
//  Surfing Spots
//
//  Created by Alessandro Graziani on 30/07/2020.
//  Copyright © 2020 Alessandro Graziani. All rights reserved.
//

import UIKit

extension UILabel {
    
    func setTextWithTransition(_ newText: String, duration: TimeInterval = 1) {
        UIView.transition(with: self, duration: duration, options: [.allowUserInteraction, .transitionFlipFromBottom], animations: {
            self.text = newText
        }, completion: nil)
    }
}
