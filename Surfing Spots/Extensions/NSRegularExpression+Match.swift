//
//  File.swift
//  Surfing Spots
//
//  Created by Alessandro Graziani on 30/07/2020.
//  Copyright © 2020 Alessandro Graziani. All rights reserved.
//

import Foundation

extension NSRegularExpression {
    convenience init(_ pattern: String) {
        do {
            try self.init(pattern: pattern)
        } catch {
            preconditionFailure("Illegal regular expression: \(pattern).")
        }
    }

    func stringMatched(in text: String) -> String? {

        let results = self.matches(in: text,
                                   range: NSRange(text.startIndex..., in: text))
        return results.compactMap {
            Range($0.range, in: text).map { String(text[$0]) }
            }.first
    }

}
