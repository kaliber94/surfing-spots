# Surfing Spots 🏄‍

Welcome to the Neato Surfing Spots challenge!

Your goal will be to build an app that helps users find the perfect spot to surf according to the local weather conditions. Beware, the weather could change easily and quickly!

🚀🚀🚀


Download the project & run **Surfing Spots** scheme to build and run the app with the cities list provided from the Api of the challenge.

The **Surfing Spots Mock** scheme instead will run the app with `MockRepository`. The `MockRepository` read city list from a local property list file, the `CityTestDataSource.plist` situated into the app, in the *Resources* folder.

Free to edit the mock city list adding or removing item, the `PhotoRepository` will download the image for any city you enter 😎

Cities images are provided by [Unsplash API](https://unsplash.com/documentation) | [API Terms](https://unsplash.com/api-terms)

**• Difference-kit branch**

In the `difference-kit` branch has been implemented the difference kit library for reloading the list. To run the app from this branch you need to run the commannd pod install from the terminal and open the Workspace.

[API Documentation](https://ra1028.github.io/DifferenceKit/)