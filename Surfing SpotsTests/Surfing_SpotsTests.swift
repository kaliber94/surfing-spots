//
//  Surfing_SpotsTests.swift
//  Surfing SpotsTests
//
//  Created by Alessandro Graziani on 29/07/2020.
//  Copyright © 2020 Alessandro Graziani. All rights reserved.
//

import XCTest
@testable import Surfing_Spots

class Surfing_SpotsTests: XCTestCase {
    
    override func setUp() {
        DependencyResolver.inject(MockMainRepository.self, for: .mainRepository)
    }
    
    func testRetrieveCityList() {
        let repository = DependencyResolver.resolve(MainRepository.self)
        
        var error: Error?
        var responseData: CitiesResponse?
        
        repository?.retrieveCities(completionHandler: { result in
            switch result {
            case .success(let cities):
                responseData = cities
            case .failure(let genericError):
                error = genericError
            }
        })
        
        XCTAssertNil(error)
        XCTAssertNotNil(responseData)
    }
    
    func testRetrieveRandomNumber() {
        let repository = DependencyResolver.resolve(MainRepository.self)
        
        var error: Error?
        var number: Int?
        
        repository?.retrieveRandomNumber(completionHandler: { result in
            switch result {
            case .success(let response):
                number = response
            case .failure(let genericError):
                error = genericError
            }
        })
        
        XCTAssertNil(error)
        XCTAssertNotNil(number)
    }
    
    func testHomeViewModel() {
        let viewModel = HomeViewModel()
        
        let expectation = self.expectation(description: "wait")
        
        var response: [City]?
        viewModel.citiesUpdating = { cities in
            response = cities
            expectation.fulfill()
        }
        
        let expectation2 = self.expectation(description: "wait2")
        
        var cityUpdated: City?
        viewModel.orderUpdating = { city in
            cityUpdated = city
            expectation2.fulfill()
        }
        
        viewModel.fetchData()
        self.wait(for: [expectation, expectation2], timeout: 10)
        
        XCTAssertNotNil(response)
        XCTAssertNotNil(cityUpdated)
        
    }

}
